package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitee.com/workits/workits-admin/component"
	"gitee.com/workits/workits-admin/internal/config"

	"gitee.com/workits/pkgs/cachex"
	"gitee.com/workits/pkgs/configx"
	"gitee.com/workits/pkgs/dbx"
	"gitee.com/workits/pkgs/logx"
	"gitee.com/workits/pkgs/utilx"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	// 读取配置文件
	if _, err := configx.UnmarshalFiles("./config", &config.G); err != nil {
		panic(err)
	}
	// 初始化日志
	logx.Init(config.G.Log)
	// 初始化缓存
	if err := cachex.InitCache(config.G.Cache); err != nil {
		panic(err)
	}
	// 初始化数据库
	if err := dbx.InitConn(config.G.DBConns...); err != nil {
		panic(err)
	}
	// 加载系统组件
	utilx.LoadComponents(component.C)
	// 阻塞主进程
	fmt.Println("service is running...")
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	<-c
}
