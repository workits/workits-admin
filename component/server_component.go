package component

import (
	_ "gitee.com/workits/workits-admin/docs"
	"gitee.com/workits/workits-admin/internal/component/router"
	"gitee.com/workits/workits-admin/internal/config"

	"gitee.com/workits/pkgs/cachex"
	"gitee.com/workits/pkgs/captchax"
	"gitee.com/workits/pkgs/serverx"
	"github.com/gin-gonic/gin"
)

func (c *Component) AddServerComponent() {
	// 初始化图片验证码引擎
	captchax.InitDigitEngine(config.G.Captcha, cachex.C)

	serverx.Run(config.G.Mode, config.G.Server, func(engine *gin.Engine) {
		// 注册路由
		group := engine.Group(
			config.G.Server.RoutePrefix,                    // 前缀
			serverx.WithJWT(config.G.Server, config.G.Jwt), // JWT身份认证
			serverx.WithCasbin(config.G.Casbin),            // Casbin权限认证
		)
		router.R.BindHandler(group)
	})
}
