-- casbin权限
create table `casbin_rule` (
    `id` bigint not null auto_increment,
    `ptype` varchar(255),
    `v0` varchar(255),
    `v1` varchar(255),
    `v2` varchar(255),
    `v3` varchar(255),
    `v4` varchar(255),
    `v5` varchar(255),
    primary key (`id`)
);

insert into `casbin_rule` (`ptype`, `v0`, `v1`, `v2`, `v3`, `v4`, `v5`) values ('p', '*', '/v1/system/security/*', 'POST', '', '', '');
insert into `casbin_rule` (`ptype`, `v0`, `v1`, `v2`, `v3`, `v4`, `v5`) values ('p', '*', '/v1/system/settings/*', 'POST', '', '', '');
insert into `casbin_rule` (`ptype`, `v0`, `v1`, `v2`, `v3`, `v4`, `v5`) values ('p', '*', '/v1/system/account/*', 'POST', '', '', '');
insert into `casbin_rule` (`ptype`, `v0`, `v1`, `v2`, `v3`, `v4`, `v5`) values ('p', '*', '/v1/system/dict/*', 'POST', '', '', '');

-- 数据字典
create table `sys_dict_type` (
    `id` bigint not null auto_increment comment '主键',
    `name` varchar(128) comment '字典名称',
    `type` varchar(32) comment '字典类型',
    `disabled` char(2) comment '是否禁用',
    `created_at` datetime comment '创建时间',
    `created_by` bigint comment '创建人',
    `updated_at` datetime comment '更新时间',
    `updated_by` bigint comment '更新人',
    `deleted_at` datetime comment '删除时间',
    `deleted_by` bigint comment '删除人',
    primary key (id)
) comment '字典类型';

create table `sys_dict_data` (
    `id` bigint not null auto_increment comment '主键',
    `type` varchar(32) comment '字典类型',
    `label` varchar(128) comment '字典标签',
    `value` varchar(255) comment '字典键值',
    `is_default` char(2) comment '是否是默认值',
    `sort_by` int comment '排序',
    `disabled` char(2) comment '是否禁用',
    `created_at` datetime comment '创建时间',
    `created_by` bigint comment '创建人',
    `updated_at` datetime comment '更新时间',
    `updated_by` bigint comment '更新人',
    `deleted_at` datetime comment '删除时间',
    `deleted_by` bigint comment '删除人',
    primary key (id)
) comment '字典数据';

-- 系统设置
create table `sys_settings` (
    `id` bigint not null auto_increment comment '主键',
    `name` varchar(128) comment '名称',
    `key` varchar(64) comment '配置key',
    `value` varchar(1024) comment '配置值',
    `disabled` char(2) comment '是否禁用',
    `created_at` datetime comment '创建时间',
    `created_by` bigint comment '创建人',
    `updated_at` datetime comment '更新时间',
    `updated_by` bigint comment '更新人',
    `deleted_at` datetime comment '删除时间',
    `deleted_by` bigint comment '删除人',
    primary key (`id`)
) comment='系统设置';

insert into `sys_settings` (`name`, `key`, `value`, `disabled`, `created_at`, `created_by`, `updated_at`, `updated_by`) values ('系统名称', 'sysName', 'Workits Admin管理系统', 'N', current_timestamp, 1, current_timestamp, 1);
insert into `sys_settings` (`name`, `key`, `value`, `disabled`, `created_at`, `created_by`, `updated_at`, `updated_by`) values ('LOGO', 'logo', 'https://blob.workits.cn/assets/workits.webp', 'N', current_timestamp, 1, current_timestamp, 1);
insert into `sys_settings` (`name`, `key`, `value`, `disabled`, `created_at`, `created_by`, `updated_at`, `updated_by`) values ('ICON', 'icon', 'https://blob.workits.cn/assets/icon.svg', 'N', current_timestamp, 1, current_timestamp, 1);
insert into `sys_settings` (`name`, `key`, `value`, `disabled`, `created_at`, `created_by`, `updated_at`, `updated_by`) values ('登录banner', 'loginBanner', 'https://blob.workits.cn/assets/banner.webp', 'N', current_timestamp, 1, current_timestamp, 1);
insert into `sys_settings` (`name`, `key`, `value`, `disabled`, `created_at`, `created_by`, `updated_at`, `updated_by`) values ('ICON类型', 'iconType', 'image/svg+xml', 'N', current_timestamp, 1, current_timestamp, 1);
insert into `sys_settings` (`name`, `key`, `value`, `disabled`, `created_at`, `created_by`, `updated_at`, `updated_by`) values ('主色调', 'primaryColor', '#165dff', 'N', current_timestamp, 1, current_timestamp, 1);
insert into `sys_settings` (`name`, `key`, `value`, `disabled`, `created_at`, `created_by`, `updated_at`, `updated_by`) values ('是否开启水印', 'enableWatermark', 'Y', 'N', current_timestamp, 1, current_timestamp, 1);
insert into `sys_settings` (`name`, `key`, `value`, `disabled`, `created_at`, `created_by`, `updated_at`, `updated_by`) values ('版权年份', 'copyrightYear', '2003', 'N', current_timestamp, 1, current_timestamp, 1);
insert into `sys_settings` (`name`, `key`, `value`, `disabled`, `created_at`, `created_by`, `updated_at`, `updated_by`) values ('版权名称', 'copyrightName', 'WORKITS.CN', 'N', current_timestamp, 1, current_timestamp, 1);
insert into `sys_settings` (`name`, `key`, `value`, `disabled`, `created_at`, `created_by`, `updated_at`, `updated_by`) values ('版权链接', 'copyrightLink', 'https://workits.cn', 'N', current_timestamp, 1, current_timestamp, 1);
insert into `sys_settings` (`name`, `key`, `value`, `disabled`, `created_at`, `created_by`, `updated_at`, `updated_by`) values ('ICP', 'icp', '京ICP备2021032888号-2', 'N', current_timestamp, 1, current_timestamp, 1);

-- 个人隐私协议
create table `sys_privacy` (
    `id` bigint not null auto_increment comment '主键',
    `version` varchar(32) comment '版本号',
    `content text` comment '协议内容',
    `ver_idx` int comment '版本索引',
    `pub_status` char(2) comment '发布状态',
    `created_at` datetime comment '创建时间',
    `created_by` bigint comment '创建人',
    `updated_at` datetime comment '更新时间',
    `updated_by` bigint comment '更新人',
    `deleted_at` datetime comment '删除时间',
    `deleted_by` bigint comment '删除人',
    primary key (id)
) comment '个人隐私协议';

insert into `sys_privacy` (`version`, `content`, `ver_idx`, `pub_status`, `created_at`, `created_by`, `updated_at`, `updated_by`) values ('1.0.0', '个人隐私政策xxxxxxxxxxxxx', 0, 'Y', current_timestamp, 1, current_timestamp, 1);

-- 系统用户
create table `sys_user` (
    `id` bigint not null auto_increment comment '主键',
    `username` varchar(128) comment '用户名',
    `password` varchar(255) comment '密码',
    `name` varchar(255) comment '姓名',
    `gender` char(2) comment '性别',
    `email` varchar(255) comment '电子邮箱',
    `phone` varchar(255) comment '手机号',
    `employee_no` varchar(32) comment '工号',
    `avatar` varchar(255) comment '头像',
    `otp_enabled` char(2) comment '是否开始otp',
    `otp_secret` varchar(32) comment 'otp密钥',
    `otp_expired` datetime comment 'otp到期时间',
    `token_expires` int comment 'token有效时长(秒)',
    `is_sys` char(2) comment '是否系统用户',
    `status` char(2) comment '状态',
    `created_at` datetime comment '创建时间',
    `created_by` bigint comment '创建人',
    `updated_at` datetime comment '更新时间',
    `updated_by` bigint comment '更新人',
    `deleted_at` datetime comment '删除时间',
    `deleted_by` bigint comment '删除人',
    primary key (`id`)
) comment='系统用户';