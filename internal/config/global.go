package config

var G = new(GlobalConfig)

type GlobalConfig struct {
	ApplicationConfig
	ServerConfig
	JwtConfig
	CasbinConfig
	CaptchaConfig
}
