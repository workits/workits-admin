package config

import "gitee.com/workits/pkgs/captchax"

type CaptchaConfig struct {
	Captcha captchax.Config `mapstructure:"captcha"`
}
