package config

import "gitee.com/workits/pkgs/jwtx"

type JwtConfig struct {
	Jwt jwtx.Config `mapstructure:"jwt"`
}
