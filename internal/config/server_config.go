package config

import (
	"gitee.com/workits/pkgs/serverx"
)

// http server配置
type ServerConfig struct {
	Server serverx.Config `mapstructure:"server"`
}
