package config

import "gitee.com/workits/pkgs/casbinx"

type CasbinConfig struct {
	Casbin casbinx.Config `mapstructure:"casbin"`
}