package config

import (
	"gitee.com/workits/pkgs/cachex"
	"gitee.com/workits/pkgs/dbx"
	"gitee.com/workits/pkgs/logx"
)

// 基础配置
type ApplicationConfig struct {
	Mode    string        `mapstructure:"mode"`
	Secret  string        `mapstructure:"mode"`
	Log     logx.Config   `mapstructure:"log"`
	Cache   cachex.Config `mapstructure:"cache"`
	DBConns []dbx.Config  `mapstructure:"db-conns"`
}
