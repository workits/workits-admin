package router

import (
	"gitee.com/workits/pkgs/serverx"
	"github.com/gin-gonic/gin"

	"gitee.com/workits/workits-admin/internal/app/system/handler"
)

func (r *Router) BindDictHandler(group *gin.RouterGroup) {
	serverx.BindHandler(group, handler.DictHandler)
}
