package router

import (
	"gitee.com/workits/pkgs/serverx"
	"github.com/gin-gonic/gin"

	"gitee.com/workits/workits-admin/internal/app/system/handler"
)

func (r *Router) BindSysSettingsHandler(group *gin.RouterGroup) {
	serverx.BindHandler(group, handler.SysSettingsHandler)
}
