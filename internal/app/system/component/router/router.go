package router

import (
	"github.com/gin-gonic/gin"
	"gitee.com/workits/pkgs/utilx"
)

var R = new(Router)

type Router struct {
}

func (r *Router) BindHandler(group *gin.RouterGroup) {
	utilx.LoadRouters(r, group)
}
