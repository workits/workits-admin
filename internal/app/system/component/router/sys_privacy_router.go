package router

import (
	"gitee.com/workits/pkgs/serverx"
	"github.com/gin-gonic/gin"

	"gitee.com/workits/workits-admin/internal/app/system/handler"
)

func (r *Router) BindSysPrivacyHandler(group *gin.RouterGroup) {
	serverx.BindHandler(group, handler.SysPrivacyHandler)
}
