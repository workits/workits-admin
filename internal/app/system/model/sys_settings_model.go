package model

import (
	"time"

	"gitee.com/workits/pkgs/modelx"
)

// SysSettingsListReqModel 查询系统设置列表参数
type SysSettingsListReqModel struct {
	modelx.ReqPageWrapper

	// TODO 添加更多查询条件
}

// SysSettingsNewReqModel 添加系统设置参数
type SysSettingsNewReqModel struct {
	NameName         string `json:"name"`     // 名称
	KeyKey           string `json:"key"`      // 配置Key
	ValueValue       string `json:"value"`    // 配置值
	DisabledDisabled string `json:"disabled"` // 是否禁用
}

// SysSettingsModReqModel 修改系统设置参数
type SysSettingsModReqModel struct {
	Id       int64  `json:"id" validate:"required"` // 主键
	Name     string `json:"name"`                   // 名称
	Key      string `json:"key"`                    // 配置Key
	Value    string `json:"value"`                  // 配置值
	Disabled string `json:"disabled"`               // 是否禁用
}

// SysSettingsModel 系统设置信息
type SysSettingsModel struct {
	Id        int64     `json:"id"`        // 主键
	Name      string    `json:"name"`      // 名称
	Key       string    `json:"key"`       // 配置Key
	Value     string    `json:"value"`     // 配置值
	Disabled  string    `json:"disabled"`  // 是否禁用
	CreatedAt time.Time `json:"createdAt"` // 创建时间
	CreatedBy int64     `json:"createdBy"` // 创建人
	UpdatedAt time.Time `json:"updatedAt"` // 更新时间
	UpdatedBy int64     `json:"updatedBy"` // 更新人
}
