package model

import (
	"time"

	"gitee.com/workits/pkgs/modelx"
)

// SysDictDataListReqModel 查询字典数据列表参数
type SysDictDataListReqModel struct {
	modelx.ReqPageWrapper

	// TODO 添加更多查询条件
}

// SysDictDataNewReqModel 添加字典数据参数
type SysDictDataNewReqModel struct {
	TypeType           string `json:"type"`      // 字典类型
	LabelLabel         string `json:"label"`     // 字典标签
	ValueValue         string `json:"value"`     // 字典键值
	IsDefaultIsDefault string `json:"isDefault"` // 是否是默认值
	SortBySortBy       int    `json:"sortBy"`    // 排序
	DisabledDisabled   string `json:"disabled"`  // 是否禁用
}

// SysDictDataModReqModel 修改字典数据参数
type SysDictDataModReqModel struct {
	Id        int64  `json:"id" validate:"required"` // 主键
	Type      string `json:"type"`                   // 字典类型
	Label     string `json:"label"`                  // 字典标签
	Value     string `json:"value"`                  // 字典键值
	IsDefault string `json:"isDefault"`              // 是否是默认值
	SortBy    int    `json:"sortBy"`                 // 排序
	Disabled  string `json:"disabled"`               // 是否禁用
}

// SysDictDataModel 字典数据信息
type SysDictDataModel struct {
	Id        int64     `json:"id"`        // 主键
	Type      string    `json:"type"`      // 字典类型
	Label     string    `json:"label"`     // 字典标签
	Value     string    `json:"value"`     // 字典键值
	IsDefault string    `json:"isDefault"` // 是否是默认值
	SortBy    int       `json:"sortBy"`    // 排序
	Disabled  string    `json:"disabled"`  // 是否禁用
	CreatedAt time.Time `json:"createdAt"` // 创建时间
	CreatedBy int64     `json:"createdBy"` // 创建人
	UpdatedAt time.Time `json:"updatedAt"` // 更新时间
	UpdatedBy int64     `json:"updatedBy"` // 更新人
}
