package model

import (
	"time"

	"gitee.com/workits/pkgs/modelx"
)

// SysPrivacyListReqModel 查询个人隐私协议列表参数
type SysPrivacyListReqModel struct {
	modelx.ReqPageWrapper

	// TODO 添加更多查询条件
}

// SysPrivacyNewReqModel 添加个人隐私协议参数
type SysPrivacyNewReqModel struct {
	VersionVersion string    `json:"version"`    // 版本号
	ContentContent string    `json:"content"`    // 协议内容
	VerIdxVerIdx   int       `json:"verIdx"`     // 版本索引
	PubTime        time.Time `json:"pub_time"`   // 发布时间
	PubStatus      string    `json:"pub_status"` // 发布状态
}

// SysPrivacyModReqModel 修改个人隐私协议参数
type SysPrivacyModReqModel struct {
	Id        int64     `json:"id" validate:"required"` // 主键
	Version   string    `json:"version"`                // 版本号
	Content   string    `json:"content"`                // 协议内容
	VerIdx    int       `json:"verIdx"`                 // 版本索引
	PubTime   time.Time `json:"pub_time"`               // 发布时间
	PubStatus string    `json:"pub_status"`             // 发布状态
}

// SysPrivacyModel 个人隐私协议信息
type SysPrivacyModel struct {
	Id        int64     `json:"id"`         // 主键
	Version   string    `json:"version"`    // 版本号
	Content   string    `json:"content"`    // 协议内容
	VerIdx    int       `json:"verIdx"`     // 版本索引
	PubTime   time.Time `json:"pub_time"`   // 发布时间
	PubStatus string    `json:"pub_status"` // 发布状态
	CreatedAt time.Time `json:"createdAt"`  // 创建时间
	CreatedBy int64     `json:"createdBy"`  // 创建人
	UpdatedAt time.Time `json:"updatedAt"`  // 更新时间
	UpdatedBy int64     `json:"updatedBy"`  // 更新人
}
