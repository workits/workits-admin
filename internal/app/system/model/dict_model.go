package model

type DictAllRespModel struct {
	Type string          `json:"type"`
	Data []DictDataModel `json:"data"`
}

type DictDataModel struct {
	Label     string `json:"label"`
	Value     string `json:"value"`
	IsDefault string `json:"isDefault,omitempty"`
}
