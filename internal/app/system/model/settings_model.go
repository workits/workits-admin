package model

import "time"

type SettingsConfigRespModel struct {
	SysName         string `json:"sysName"`         // 系统名称
	Logo            string `json:"logo"`            // LOGO
	Icon            string `json:"icon"`            // ICON
	LoginBanner     string `json:"loginBanner"`     // 登录banner
	IconType        string `json:"iconType"`        // ICON类型
	PrimaryColor    string `json:"primaryColor"`    // 主色调
	EnableWatermark string `json:"enableWatermark"` // 是否开启水印
	CopyrightYear   string `json:"copyrightYear"`   // 版权年份
	CopyrightName   string `json:"copyrightName"`   // 版权名称
	CopyrightLink   string `json:"copyrightLink"`   // 版权链接
	ICP             string `json:"icp"`             // ICP
}

type SettingsPrivacyReqModel struct {
	Version string `json:"version"` // 版本号
}

type SettingsPrivacyRespModel struct {
	Version string    `json:"version"` // 版本号
	Content string    `json:"content"` // 协议内容
	PubTime time.Time `json:"pubTime"` // 更新时间
	History []string  `json:"history"` // 历史记录
}
