package model

import (
	"time"

	"gitee.com/workits/pkgs/modelx"
)

// SysDictTypeListReqModel 查询字典类型列表参数
type SysDictTypeListReqModel struct {
	modelx.ReqPageWrapper

	// TODO 添加更多查询条件
}

// SysDictTypeNewReqModel 添加字典类型参数
type SysDictTypeNewReqModel struct {
	NameName         string `json:"name"`     // 字典名称
	TypeType         string `json:"type"`     // 字典类型
	DisabledDisabled string `json:"disabled"` // 是否禁用
}

// SysDictTypeModReqModel 修改字典类型参数
type SysDictTypeModReqModel struct {
	Id       int64  `json:"id" validate:"required"` // 主键
	Name     string `json:"name"`                   // 字典名称
	Type     string `json:"type"`                   // 字典类型
	Disabled string `json:"disabled"`               // 是否禁用
}

// SysDictTypeModel 字典类型信息
type SysDictTypeModel struct {
	Id        int64     `json:"id"`        // 主键
	Name      string    `json:"name"`      // 字典名称
	Type      string    `json:"type"`      // 字典类型
	Disabled  string    `json:"disabled"`  // 是否禁用
	CreatedAt time.Time `json:"createdAt"` // 创建时间
	CreatedBy int64     `json:"createdBy"` // 创建人
	UpdatedAt time.Time `json:"updatedAt"` // 更新时间
	UpdatedBy int64     `json:"updatedBy"` // 更新人
}
