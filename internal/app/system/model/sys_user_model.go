package model

import (
	"time"

	"gitee.com/workits/pkgs/modelx"
)

// SysUserListReqModel 查询系统用户列表参数
type SysUserListReqModel struct {
	modelx.ReqPageWrapper

	// TODO 添加更多查询条件
}

// SysUserNewReqModel 添加系统用户参数
type SysUserNewReqModel struct {
	UsernameUsername         string    `json:"username"`     // 用户名
	PasswordPassword         string    `json:"password"`     // 密码
	NameName                 string    `json:"name"`         // 姓名
	GenderGender             string    `json:"gender"`       // 性别
	EmailEmail               string    `json:"email"`        // 电子邮箱
	PhonePhone               string    `json:"phone"`        // 手机号
	EmployeeNoEmployeeNo     string    `json:"employeeNo"`   // 工号
	AvatarAvatar             string    `json:"avatar"`       // 头像
	OtpEnabledOtpEnabled     string    `json:"otpEnabled"`   // 是否开始OTP
	OtpSecretOtpSecret       string    `json:"otpSecret"`    // OTP密钥
	OtpExpiredOtpExpired     time.Time `json:"otpExpired"`   // OTP到期时间
	TokenExpiresTokenExpires int       `json:"tokenExpires"` // Token有效时长(秒)
	IsSysIsSys               string    `json:"isSys"`        // 是否系统用户
	StatusStatus             string    `json:"status"`       // 状态
}

// SysUserModReqModel 修改系统用户参数
type SysUserModReqModel struct {
	Id           int64     `json:"id" validate:"required"` // 主键
	Username     string    `json:"username"`               // 用户名
	Password     string    `json:"password"`               // 密码
	Name         string    `json:"name"`                   // 姓名
	Gender       string    `json:"gender"`                 // 性别
	Email        string    `json:"email"`                  // 电子邮箱
	Phone        string    `json:"phone"`                  // 手机号
	EmployeeNo   string    `json:"employeeNo"`             // 工号
	Avatar       string    `json:"avatar"`                 // 头像
	OtpEnabled   string    `json:"otpEnabled"`             // 是否开始OTP
	OtpSecret    string    `json:"otpSecret"`              // OTP密钥
	OtpExpired   time.Time `json:"otpExpired"`             // OTP到期时间
	TokenExpires int       `json:"tokenExpires"`           // Token有效时长(秒)
	IsSys        string    `json:"isSys"`                  // 是否系统用户
	Status       string    `json:"status"`                 // 状态
}

// SysUserModel 系统用户信息
type SysUserModel struct {
	Id           int64     `json:"id"`           // 主键
	Username     string    `json:"username"`     // 用户名
	Password     string    `json:"password"`     // 密码
	Name         string    `json:"name"`         // 姓名
	Gender       string    `json:"gender"`       // 性别
	Email        string    `json:"email"`        // 电子邮箱
	Phone        string    `json:"phone"`        // 手机号
	EmployeeNo   string    `json:"employeeNo"`   // 工号
	Avatar       string    `json:"avatar"`       // 头像
	OtpEnabled   string    `json:"otpEnabled"`   // 是否开始OTP
	OtpSecret    string    `json:"otpSecret"`    // OTP密钥
	OtpExpired   time.Time `json:"otpExpired"`   // OTP到期时间
	TokenExpires int       `json:"tokenExpires"` // Token有效时长(秒)
	IsSys        string    `json:"isSys"`        // 是否系统用户
	Status       string    `json:"status"`       // 状态
	CreatedAt    time.Time `json:"createdAt"`    // 创建时间
	CreatedBy    int64     `json:"createdBy"`    // 创建人
	UpdatedAt    time.Time `json:"updatedAt"`    // 更新时间
	UpdatedBy    int64     `json:"updatedBy"`    // 更新人
}
