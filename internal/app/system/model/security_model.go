package model

type SecurityCaptchaRespModel struct {
	Id  string `json:"id"`                  // 验证码识别ID
	Img string `json:"img" format:"base64"` // base64图片
}
