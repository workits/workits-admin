package entity

import (
	"time"

	"gitee.com/workits/pkgs/dbx"
)

// SysDictDataTableName 字典数据表名
const SysDictDataTableName = "sys_dict_data"

// SysDictDataEntity 字典数据
type SysDictDataEntity struct {
	dbx.CommonEntity

	Type      string `column:"type"`       // 字典类型
	Label     string `column:"label"`      // 字典标签
	Value     string `column:"value"`      // 字典键值
	IsDefault string `column:"is_default"` // 是否是默认值
	SortBy    int    `column:"sort_by"`    // 排序
	Disabled  string `column:"disabled"`   // 是否禁用
}

// GetTableName 获取表名
func (entity *SysDictDataEntity) GetTableName() string {
	return SysDictDataTableName
}

// GetDefaultValue 设置默认值
func (entity *SysDictDataEntity) GetDefaultValue() map[string]any {
	return map[string]any{
		"Type":      nil,
		"Label":     nil,
		"Value":     nil,
		"IsDefault": nil,
		"SortBy":    nil,
		"Disabled":  nil,
		"CreatedAt": time.Now(),
		"CreatedBy": nil,
		"UpdatedAt": time.Now(),
		"UpdatedBy": nil,
		"DeletedAt": nil,
		"DeletedBy": nil,
	}
}
