package entity

import (
	"time"

	"gitee.com/workits/pkgs/dbx"
)

// SysUserTableName 系统用户表名
const SysUserTableName = "sys_user"

// SysUserEntity 系统用户
type SysUserEntity struct {
	dbx.CommonEntity

	Username     string    `column:"username"`      // 用户名
	Password     string    `column:"password"`      // 密码
	Name         string    `column:"name"`          // 姓名
	Gender       string    `column:"gender"`        // 性别
	Email        string    `column:"email"`         // 电子邮箱
	Phone        string    `column:"phone"`         // 手机号
	EmployeeNo   string    `column:"employee_no"`   // 工号
	Avatar       string    `column:"avatar"`        // 头像
	OtpEnabled   string    `column:"otp_enabled"`   // 是否开始OTP
	OtpSecret    string    `column:"otp_secret"`    // OTP密钥
	OtpExpired   time.Time `column:"otp_expired"`   // OTP到期时间
	TokenExpires int       `column:"token_expires"` // Token有效时长(秒)
	IsSys        string    `column:"is_sys"`        // 是否系统用户
	Status       string    `column:"status"`        // 状态
}

// GetTableName 获取表名
func (entity *SysUserEntity) GetTableName() string {
	return SysUserTableName
}

// GetDefaultValue 设置默认值
func (entity *SysUserEntity) GetDefaultValue() map[string]any {
	return map[string]any{
		"Username":     nil,
		"Password":     nil,
		"Name":         nil,
		"Gender":       nil,
		"Email":        nil,
		"Phone":        nil,
		"EmployeeNo":   nil,
		"Avatar":       nil,
		"OtpEnabled":   nil,
		"OtpSecret":    nil,
		"OtpExpired":   nil,
		"TokenExpires": nil,
		"IsSys":        nil,
		"Status":       nil,
		"CreatedAt":    time.Now(),
		"CreatedBy":    nil,
		"UpdatedAt":    time.Now(),
		"UpdatedBy":    nil,
		"DeletedAt":    nil,
		"DeletedBy":    nil,
	}
}
