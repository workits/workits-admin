package entity

import (
	"time"

	"gitee.com/workits/pkgs/dbx"
)

// SysDictTypeTableName 字典类型表名
const SysDictTypeTableName = "sys_dict_type"

// SysDictTypeEntity 字典类型
type SysDictTypeEntity struct {
	dbx.CommonEntity

	Name     string `column:"name"`     // 字典名称
	Type     string `column:"type"`     // 字典类型
	Disabled string `column:"disabled"` // 是否禁用
}

// GetTableName 获取表名
func (entity *SysDictTypeEntity) GetTableName() string {
	return SysDictTypeTableName
}

// GetDefaultValue 设置默认值
func (entity *SysDictTypeEntity) GetDefaultValue() map[string]any {
	return map[string]any{
		"Name":      nil,
		"Type":      nil,
		"Disabled":  nil,
		"CreatedAt": time.Now(),
		"CreatedBy": nil,
		"UpdatedAt": time.Now(),
		"UpdatedBy": nil,
		"DeletedAt": nil,
		"DeletedBy": nil,
	}
}
