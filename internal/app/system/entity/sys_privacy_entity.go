package entity

import (
	"time"

	"gitee.com/workits/pkgs/dbx"
)

// SysPrivacyTableName 个人隐私协议表名
const SysPrivacyTableName = "sys_privacy"

// SysPrivacyEntity 个人隐私协议
type SysPrivacyEntity struct {
	dbx.CommonEntity

	Version   string    `column:"version"`    // 版本号
	Content   string    `column:"content"`    // 协议内容
	VerIdx    int       `column:"ver_idx"`    // 版本索引
	PubTime   time.Time `column:"pub_time"`   // 发布时间
	PubStatus string    `column:"pub_status"` // 发布状态
}

// GetTableName 获取表名
func (entity *SysPrivacyEntity) GetTableName() string {
	return SysPrivacyTableName
}

// GetDefaultValue 设置默认值
func (entity *SysPrivacyEntity) GetDefaultValue() map[string]any {
	return map[string]any{
		"Version":   nil,
		"Content":   nil,
		"VerIdx":    nil,
		"PubStatus": nil,
		"CreatedAt": time.Now(),
		"CreatedBy": nil,
		"UpdatedAt": time.Now(),
		"UpdatedBy": nil,
		"DeletedAt": nil,
		"DeletedBy": nil,
	}
}
