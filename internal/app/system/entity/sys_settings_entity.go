package entity

import (
	"time"

	"gitee.com/workits/pkgs/dbx"
)

// SysSettingsTableName 系统设置表名
const SysSettingsTableName = "sys_settings"

// SysSettingsEntity 系统设置
type SysSettingsEntity struct {
	dbx.CommonEntity

	Name     string `column:"name"`     // 名称
	Key      string `column:"key"`      // 配置Key
	Value    string `column:"value"`    // 配置值
	Disabled string `column:"disabled"` // 是否禁用
}

// GetTableName 获取表名
func (entity *SysSettingsEntity) GetTableName() string {
	return SysSettingsTableName
}

// GetDefaultValue 设置默认值
func (entity *SysSettingsEntity) GetDefaultValue() map[string]any {
	return map[string]any{
		"Name":      nil,
		"Key":       nil,
		"Value":     nil,
		"Disabled":  nil,
		"CreatedAt": time.Now(),
		"CreatedBy": nil,
		"UpdatedAt": time.Now(),
		"UpdatedBy": nil,
		"DeletedAt": nil,
		"DeletedBy": nil,
	}
}
