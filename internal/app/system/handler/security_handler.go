package handler

import (
	"context"

	"gitee.com/workits/pkgs/captchax"
	"gitee.com/workits/workits-admin/internal/app/system/model"
	"github.com/pkg/errors"
)

var SecurityHandler = new(securityHandler)

type securityHandler struct {
}

// Captcha 图片验证码
//
//	@Summary	图片验证码
//	@Tags		system
//	@Produce	json
//	@Success	200	{object}	ginx.RespWrapper{data=model.SecurityCaptchaRespModel}
//	@Router		/system/security/captcha [post]
func (h *securityHandler) Captcha(ctx context.Context) (*model.SecurityCaptchaRespModel, error) {
	// 生成验证码
	id, b64s, err := captchax.DigitCaptcha.Generate()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return &model.SecurityCaptchaRespModel{
		Id:  id,
		Img: b64s,
	}, nil
}
