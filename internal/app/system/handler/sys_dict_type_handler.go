package handler

import (
	"context"
	"time"

	"gitee.com/chunanyong/zorm"
	"gitee.com/workits/pkgs/contextx"
	"gitee.com/workits/pkgs/dbx"
	"gitee.com/workits/pkgs/errorx"
	"gitee.com/workits/pkgs/modelx"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"

	"gitee.com/workits/workits-admin/internal/app/system/entity"
	"gitee.com/workits/workits-admin/internal/app/system/model"
)

// SysDictTypeHandler 字典类型处理器
var SysDictTypeHandler = new(sysDictTypeHandler)

type sysDictTypeHandler struct {
}

// List 字典类型列表
//
//	@Summary	字典类型列表
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		model.SysDictTypeListReqModel	true	"字典类型列表参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.RespPageModel{list=model.SysDictTypeModel}}
//	@Router		/system/sysDictType/list [post]
func (h *sysDictTypeHandler) List(ctx context.Context, reqModel *model.SysDictTypeListReqModel) (*modelx.RespPageModel, error) {
	// 构造查询条件
	finder := zorm.NewSelectFinder(entity.SysDictTypeTableName)
	finder.Append("where deleted_at is null")
	// TODO 在这里添加更多查询条件
	finder.Append("order by created_at asc")

	// 构造分页器
	page := zorm.NewPage()
	if reqModel.PageNo > 0 {
		page.PageNo = reqModel.PageNo
	}
	if reqModel.PageSize > 0 {
		page.PageSize = reqModel.PageSize
	}

	// 执行查询
	var items []entity.SysDictTypeEntity
	if err := zorm.Query(ctx, finder, &items, page); err != nil {
		return nil, errors.WithStack(err)
	}

	// 数据类型转换
	var list []model.SysDictTypeModel
	for _, item := range items {
		var modelItem model.SysDictTypeModel
		if err := copier.Copy(&modelItem, &item); err != nil {
			return nil, errors.WithStack(err)
		}
		list = append(list, modelItem)
	}

	// 返回结果
	return &modelx.RespPageModel{
		Total: page.TotalCount,
		List:  list,
	}, nil
}

// Get 字典类型详情
//
//	@Summary	字典类型详情
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		modelx.IDModel	true	"字典类型详情参数"
//	@Success	200		{object}	modelx.RespWrapper{data=model.SysDictTypeModel}
//	@Router		/system/sysDictType/get [post]
func (h *sysDictTypeHandler) Get(ctx context.Context, reqModel *modelx.IDModel) (*model.SysDictTypeModel, error) {
	// 构建查询条件
	finder := zorm.NewSelectFinder(entity.SysDictTypeTableName)
	finder.Append("where deleted_at is null")
	finder.Append("and id = ?", reqModel.Id)

	// 执行查询
	var item entity.SysDictTypeEntity
	if has, err := zorm.QueryRow(ctx, finder, &item); err != nil {
		return nil, errors.WithStack(err)
	} else if !has {
		return nil, nil
	}

	// 数据类型转换
	var modelItem model.SysDictTypeModel
	if err := copier.Copy(&modelItem, &item); err != nil {
		return nil, errors.WithStack(err)
	}

	return &modelItem, nil
}

// New 新建字典类型
//
//	@Summary	新建字典类型
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		model.SysDictTypeNewReqModel	true	"新建字典类型参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.IDModel}
//	@Router		/system/sysDictType/new [post]
func (h *sysDictTypeHandler) New(ctx context.Context, reqModel *model.SysDictTypeNewReqModel) (*modelx.IDModel, error) {
	// 获取登录用户信息
	uid, ok := contextx.GetTraceUserId(ctx).(int64)
	if !ok {
		return nil, errorx.ErrNonLogin
	}

	// 开启事务
	newId, err := zorm.Transaction(ctx, func(ctx context.Context) (any, error) {
		// 数据类型转换
		var item entity.SysDictTypeEntity
		if err := copier.Copy(&item, reqModel); err != nil {
			return nil, errors.WithStack(err)
		}

		// 默认值
		item.CreatedBy = uid
		item.UpdatedBy = uid

		// 插入数据
		if _, err := zorm.Insert(ctx, &item); err != nil {
			return nil, errors.WithStack(err)
		}

		return item.Id, nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// 返回主健
	return &modelx.IDModel{Id: newId.(int64)}, nil
}

// Mod 修改字典类型
//
//	@Summary	修改字典类型
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		model.SysDictTypeModReqModel	true	"修改字典类型参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.RespOptModel}
//	@Router		/system/sysDictType/mod [post]
func (h *sysDictTypeHandler) Mod(ctx context.Context, reqModel *model.SysDictTypeModReqModel) (*modelx.RespOptModel, error) {
	// 获取登录用户信息
	uid, ok := contextx.GetTraceUserId(ctx).(int64)
	if !ok {
		return nil, errorx.ErrNonLogin
	}

	// 开启事务
	num, err := zorm.Transaction(ctx, func(ctx context.Context) (any, error) {
		// 数据类型转换
		var item entity.SysDictTypeEntity
		if err := copier.Copy(&item, reqModel); err != nil {
			return nil, errors.WithStack(err)
		}

		// 默认值
		item.UpdatedAt = time.Now()
		item.UpdatedBy = uid

		// 更新数据
		num, err := zorm.UpdateNotZeroValue(ctx, &item)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		return num, nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// 返回影响数据条数
	return &modelx.RespOptModel{Num: num.(int)}, nil
}

// Del 删除字典类型
//
//	@Summary	删除字典类型
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		modelx.IDModel	true	"删除字典类型参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.RespOptModel}
//	@Router		/system/sysDictType/del [post]
func (h *sysDictTypeHandler) Del(ctx context.Context, reqModel *modelx.IDModel) (*modelx.RespOptModel, error) {
	// 获取登录用户信息
	uid, ok := contextx.GetTraceUserId(ctx).(int64)
	if !ok {
		return nil, errorx.ErrNonLogin
	}

	// 开启事务
	num, err := zorm.Transaction(ctx, func(ctx context.Context) (any, error) {
		// 构造逻辑删除数据
		item := entity.SysDictTypeEntity{
			CommonEntity: dbx.CommonEntity{
				Id:        reqModel.Id,
				DeletedAt: time.Now(),
				DeletedBy: uid,
			},
		}

		// 更新数据
		num, err := zorm.UpdateNotZeroValue(ctx, &item)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		return num, nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// 返回影响数据条数
	return &modelx.RespOptModel{Num: num.(int)}, nil
}
