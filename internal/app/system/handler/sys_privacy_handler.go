package handler

import (
	"context"
	"time"

	"gitee.com/chunanyong/zorm"
	"gitee.com/workits/pkgs/contextx"
	"gitee.com/workits/pkgs/dbx"
	"gitee.com/workits/pkgs/errorx"
	"gitee.com/workits/pkgs/modelx"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"

	"gitee.com/workits/workits-admin/internal/app/system/entity"
	"gitee.com/workits/workits-admin/internal/app/system/model"
)

// SysPrivacyHandler 个人隐私协议处理器
var SysPrivacyHandler = new(sysPrivacyHandler)

type sysPrivacyHandler struct {
}

// List 个人隐私协议列表
//
//	@Summary	个人隐私协议列表
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		model.SysPrivacyListReqModel	true	"个人隐私协议列表参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.RespPageModel{list=model.SysPrivacyModel}}
//	@Router		/system/sysPrivacy/list [post]
func (h *sysPrivacyHandler) List(ctx context.Context, reqModel *model.SysPrivacyListReqModel) (*modelx.RespPageModel, error) {
	// 构造查询条件
	finder := zorm.NewSelectFinder(entity.SysPrivacyTableName)
	finder.Append("where deleted_at is null")
	// TODO 在这里添加更多查询条件
	finder.Append("order by created_at asc")

	// 构造分页器
	page := zorm.NewPage()
	if reqModel.PageNo > 0 {
		page.PageNo = reqModel.PageNo
	}
	if reqModel.PageSize > 0 {
		page.PageSize = reqModel.PageSize
	}

	// 执行查询
	var items []entity.SysPrivacyEntity
	if err := zorm.Query(ctx, finder, &items, page); err != nil {
		return nil, errors.WithStack(err)
	}

	// 数据类型转换
	var list []model.SysPrivacyModel
	for _, item := range items {
		var modelItem model.SysPrivacyModel
		if err := copier.Copy(&modelItem, &item); err != nil {
			return nil, errors.WithStack(err)
		}
		list = append(list, modelItem)
	}

	// 返回结果
	return &modelx.RespPageModel{
		Total: page.TotalCount,
		List:  list,
	}, nil
}

// Get 个人隐私协议详情
//
//	@Summary	个人隐私协议详情
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		modelx.IDModel	true	"个人隐私协议详情参数"
//	@Success	200		{object}	modelx.RespWrapper{data=model.SysPrivacyModel}
//	@Router		/system/sysPrivacy/get [post]
func (h *sysPrivacyHandler) Get(ctx context.Context, reqModel *modelx.IDModel) (*model.SysPrivacyModel, error) {
	// 构建查询条件
	finder := zorm.NewSelectFinder(entity.SysPrivacyTableName)
	finder.Append("where deleted_at is null")
	finder.Append("and id = ?", reqModel.Id)

	// 执行查询
	var item entity.SysPrivacyEntity
	if has, err := zorm.QueryRow(ctx, finder, &item); err != nil {
		return nil, errors.WithStack(err)
	} else if !has {
		return nil, nil
	}

	// 数据类型转换
	var modelItem model.SysPrivacyModel
	if err := copier.Copy(&modelItem, &item); err != nil {
		return nil, errors.WithStack(err)
	}

	return &modelItem, nil
}

// New 新建个人隐私协议
//
//	@Summary	新建个人隐私协议
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		model.SysPrivacyNewReqModel	true	"新建个人隐私协议参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.IDModel}
//	@Router		/system/sysPrivacy/new [post]
func (h *sysPrivacyHandler) New(ctx context.Context, reqModel *model.SysPrivacyNewReqModel) (*modelx.IDModel, error) {
	// 获取登录用户信息
	uid, ok := contextx.GetTraceUserId(ctx).(int64)
	if !ok {
		return nil, errorx.ErrNonLogin
	}

	// 开启事务
	newId, err := zorm.Transaction(ctx, func(ctx context.Context) (any, error) {
		// 数据类型转换
		var item entity.SysPrivacyEntity
		if err := copier.Copy(&item, reqModel); err != nil {
			return nil, errors.WithStack(err)
		}

		// 默认值
		item.CreatedBy = uid
		item.UpdatedBy = uid

		// 插入数据
		if _, err := zorm.Insert(ctx, &item); err != nil {
			return nil, errors.WithStack(err)
		}

		return item.Id, nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// 返回主健
	return &modelx.IDModel{Id: newId.(int64)}, nil
}

// Mod 修改个人隐私协议
//
//	@Summary	修改个人隐私协议
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		model.SysPrivacyModReqModel	true	"修改个人隐私协议参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.RespOptModel}
//	@Router		/system/sysPrivacy/mod [post]
func (h *sysPrivacyHandler) Mod(ctx context.Context, reqModel *model.SysPrivacyModReqModel) (*modelx.RespOptModel, error) {
	// 获取登录用户信息
	uid, ok := contextx.GetTraceUserId(ctx).(int64)
	if !ok {
		return nil, errorx.ErrNonLogin
	}

	// 开启事务
	num, err := zorm.Transaction(ctx, func(ctx context.Context) (any, error) {
		// 数据类型转换
		var item entity.SysPrivacyEntity
		if err := copier.Copy(&item, reqModel); err != nil {
			return nil, errors.WithStack(err)
		}

		// 默认值
		item.UpdatedAt = time.Now()
		item.UpdatedBy = uid

		// 更新数据
		num, err := zorm.UpdateNotZeroValue(ctx, &item)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		return num, nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// 返回影响数据条数
	return &modelx.RespOptModel{Num: num.(int)}, nil
}

// Del 删除个人隐私协议
//
//	@Summary	删除个人隐私协议
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		modelx.IDModel	true	"删除个人隐私协议参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.RespOptModel}
//	@Router		/system/sysPrivacy/del [post]
func (h *sysPrivacyHandler) Del(ctx context.Context, reqModel *modelx.IDModel) (*modelx.RespOptModel, error) {
	// 获取登录用户信息
	uid, ok := contextx.GetTraceUserId(ctx).(int64)
	if !ok {
		return nil, errorx.ErrNonLogin
	}

	// 开启事务
	num, err := zorm.Transaction(ctx, func(ctx context.Context) (any, error) {
		// 构造逻辑删除数据
		item := entity.SysPrivacyEntity{
			CommonEntity: dbx.CommonEntity{
				Id:        reqModel.Id,
				DeletedAt: time.Now(),
				DeletedBy: uid,
			},
		}

		// 更新数据
		num, err := zorm.UpdateNotZeroValue(ctx, &item)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		return num, nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// 返回影响数据条数
	return &modelx.RespOptModel{Num: num.(int)}, nil
}
