package handler

import (
	"context"
	"time"

	"gitee.com/chunanyong/zorm"
	"gitee.com/workits/pkgs/contextx"
	"gitee.com/workits/pkgs/dbx"
	"gitee.com/workits/pkgs/errorx"
	"gitee.com/workits/pkgs/modelx"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"

	"gitee.com/workits/workits-admin/internal/app/system/entity"
	"gitee.com/workits/workits-admin/internal/app/system/model"
)

// SysSettingsHandler 系统设置处理器
var SysSettingsHandler = new(sysSettingsHandler)

type sysSettingsHandler struct {
}

// List 系统设置列表
//
//	@Summary	系统设置列表
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		model.SysSettingsListReqModel	true	"系统设置列表参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.RespPageModel{list=model.SysSettingsModel}}
//	@Router		/system/sysSettings/list [post]
func (h *sysSettingsHandler) List(ctx context.Context, reqModel *model.SysSettingsListReqModel) (*modelx.RespPageModel, error) {
	// 构造查询条件
	finder := zorm.NewSelectFinder(entity.SysSettingsTableName)
	finder.Append("where deleted_at is null")
	// TODO 在这里添加更多查询条件
	finder.Append("order by created_at asc")

	// 构造分页器
	page := zorm.NewPage()
	if reqModel.PageNo > 0 {
		page.PageNo = reqModel.PageNo
	}
	if reqModel.PageSize > 0 {
		page.PageSize = reqModel.PageSize
	}

	// 执行查询
	var items []entity.SysSettingsEntity
	if err := zorm.Query(ctx, finder, &items, page); err != nil {
		return nil, errors.WithStack(err)
	}

	// 数据类型转换
	var list []model.SysSettingsModel
	for _, item := range items {
		var modelItem model.SysSettingsModel
		if err := copier.Copy(&modelItem, &item); err != nil {
			return nil, errors.WithStack(err)
		}
		list = append(list, modelItem)
	}

	// 返回结果
	return &modelx.RespPageModel{
		Total: page.TotalCount,
		List:  list,
	}, nil
}

// Get 系统设置详情
//
//	@Summary	系统设置详情
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		modelx.IDModel	true	"系统设置详情参数"
//	@Success	200		{object}	modelx.RespWrapper{data=model.SysSettingsModel}
//	@Router		/system/sysSettings/get [post]
func (h *sysSettingsHandler) Get(ctx context.Context, reqModel *modelx.IDModel) (*model.SysSettingsModel, error) {
	// 构建查询条件
	finder := zorm.NewSelectFinder(entity.SysSettingsTableName)
	finder.Append("where deleted_at is null")
	finder.Append("and id = ?", reqModel.Id)

	// 执行查询
	var item entity.SysSettingsEntity
	if has, err := zorm.QueryRow(ctx, finder, &item); err != nil {
		return nil, errors.WithStack(err)
	} else if !has {
		return nil, nil
	}

	// 数据类型转换
	var modelItem model.SysSettingsModel
	if err := copier.Copy(&modelItem, &item); err != nil {
		return nil, errors.WithStack(err)
	}

	return &modelItem, nil
}

// New 新建系统设置
//
//	@Summary	新建系统设置
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		model.SysSettingsNewReqModel	true	"新建系统设置参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.IDModel}
//	@Router		/system/sysSettings/new [post]
func (h *sysSettingsHandler) New(ctx context.Context, reqModel *model.SysSettingsNewReqModel) (*modelx.IDModel, error) {
	// 获取登录用户信息
	uid, ok := contextx.GetTraceUserId(ctx).(int64)
	if !ok {
		return nil, errorx.ErrNonLogin
	}

	// 开启事务
	newId, err := zorm.Transaction(ctx, func(ctx context.Context) (any, error) {
		// 数据类型转换
		var item entity.SysSettingsEntity
		if err := copier.Copy(&item, reqModel); err != nil {
			return nil, errors.WithStack(err)
		}

		// 默认值
		item.CreatedBy = uid
		item.UpdatedBy = uid

		// 插入数据
		if _, err := zorm.Insert(ctx, &item); err != nil {
			return nil, errors.WithStack(err)
		}

		return item.Id, nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// 返回主健
	return &modelx.IDModel{Id: newId.(int64)}, nil
}

// Mod 修改系统设置
//
//	@Summary	修改系统设置
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		model.SysSettingsModReqModel	true	"修改系统设置参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.RespOptModel}
//	@Router		/system/sysSettings/mod [post]
func (h *sysSettingsHandler) Mod(ctx context.Context, reqModel *model.SysSettingsModReqModel) (*modelx.RespOptModel, error) {
	// 获取登录用户信息
	uid, ok := contextx.GetTraceUserId(ctx).(int64)
	if !ok {
		return nil, errorx.ErrNonLogin
	}

	// 开启事务
	num, err := zorm.Transaction(ctx, func(ctx context.Context) (any, error) {
		// 数据类型转换
		var item entity.SysSettingsEntity
		if err := copier.Copy(&item, reqModel); err != nil {
			return nil, errors.WithStack(err)
		}

		// 默认值
		item.UpdatedAt = time.Now()
		item.UpdatedBy = uid

		// 更新数据
		num, err := zorm.UpdateNotZeroValue(ctx, &item)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		return num, nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// 返回影响数据条数
	return &modelx.RespOptModel{Num: num.(int)}, nil
}

// Del 删除系统设置
//
//	@Summary	删除系统设置
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		modelx.IDModel	true	"删除系统设置参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.RespOptModel}
//	@Router		/system/sysSettings/del [post]
func (h *sysSettingsHandler) Del(ctx context.Context, reqModel *modelx.IDModel) (*modelx.RespOptModel, error) {
	// 获取登录用户信息
	uid, ok := contextx.GetTraceUserId(ctx).(int64)
	if !ok {
		return nil, errorx.ErrNonLogin
	}

	// 开启事务
	num, err := zorm.Transaction(ctx, func(ctx context.Context) (any, error) {
		// 构造逻辑删除数据
		item := entity.SysSettingsEntity{
			CommonEntity: dbx.CommonEntity{
				Id:        reqModel.Id,
				DeletedAt: time.Now(),
				DeletedBy: uid,
			},
		}

		// 更新数据
		num, err := zorm.UpdateNotZeroValue(ctx, &item)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		return num, nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// 返回影响数据条数
	return &modelx.RespOptModel{Num: num.(int)}, nil
}
