package handler

import (
	"context"
	"time"

	"gitee.com/chunanyong/zorm"
	"gitee.com/workits/pkgs/contextx"
	"gitee.com/workits/pkgs/dbx"
	"gitee.com/workits/pkgs/errorx"
	"gitee.com/workits/pkgs/modelx"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"

	"gitee.com/workits/workits-admin/internal/app/system/entity"
	"gitee.com/workits/workits-admin/internal/app/system/model"
)

// SysUserHandler 系统用户处理器
var SysUserHandler = new(sysUserHandler)

type sysUserHandler struct {
}

// List 系统用户列表
//
//	@Summary	系统用户列表
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		model.SysUserListReqModel	true	"系统用户列表参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.RespPageModel{list=model.SysUserModel}}
//	@Router		/system/sysUser/list [post]
func (h *sysUserHandler) List(ctx context.Context, reqModel *model.SysUserListReqModel) (*modelx.RespPageModel, error) {
	// 构造查询条件
	finder := zorm.NewSelectFinder(entity.SysUserTableName)
	finder.Append("where deleted_at is null")
	// TODO 在这里添加更多查询条件
	finder.Append("order by created_at asc")

	// 构造分页器
	page := zorm.NewPage()
	if reqModel.PageNo > 0 {
		page.PageNo = reqModel.PageNo
	}
	if reqModel.PageSize > 0 {
		page.PageSize = reqModel.PageSize
	}

	// 执行查询
	var items []entity.SysUserEntity
	if err := zorm.Query(ctx, finder, &items, page); err != nil {
		return nil, errors.WithStack(err)
	}

	// 数据类型转换
	var list []model.SysUserModel
	for _, item := range items {
		var modelItem model.SysUserModel
		if err := copier.Copy(&modelItem, &item); err != nil {
			return nil, errors.WithStack(err)
		}
		list = append(list, modelItem)
	}

	// 返回结果
	return &modelx.RespPageModel{
		Total: page.TotalCount,
		List:  list,
	}, nil
}

// Get 系统用户详情
//
//	@Summary	系统用户详情
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		modelx.IDModel	true	"系统用户详情参数"
//	@Success	200		{object}	modelx.RespWrapper{data=model.SysUserModel}
//	@Router		/system/sysUser/get [post]
func (h *sysUserHandler) Get(ctx context.Context, reqModel *modelx.IDModel) (*model.SysUserModel, error) {
	// 构建查询条件
	finder := zorm.NewSelectFinder(entity.SysUserTableName)
	finder.Append("where deleted_at is null")
	finder.Append("and id = ?", reqModel.Id)

	// 执行查询
	var item entity.SysUserEntity
	if has, err := zorm.QueryRow(ctx, finder, &item); err != nil {
		return nil, errors.WithStack(err)
	} else if !has {
		return nil, nil
	}

	// 数据类型转换
	var modelItem model.SysUserModel
	if err := copier.Copy(&modelItem, &item); err != nil {
		return nil, errors.WithStack(err)
	}

	return &modelItem, nil
}

// New 新建系统用户
//
//	@Summary	新建系统用户
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		model.SysUserNewReqModel	true	"新建系统用户参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.IDModel}
//	@Router		/system/sysUser/new [post]
func (h *sysUserHandler) New(ctx context.Context, reqModel *model.SysUserNewReqModel) (*modelx.IDModel, error) {
	// 获取登录用户信息
	uid, ok := contextx.GetTraceUserId(ctx).(int64)
	if !ok {
		return nil, errorx.ErrNonLogin
	}

	// 开启事务
	newId, err := zorm.Transaction(ctx, func(ctx context.Context) (any, error) {
		// 数据类型转换
		var item entity.SysUserEntity
		if err := copier.Copy(&item, reqModel); err != nil {
			return nil, errors.WithStack(err)
		}

		// 默认值
		item.CreatedBy = uid
		item.UpdatedBy = uid

		// 插入数据
		if _, err := zorm.Insert(ctx, &item); err != nil {
			return nil, errors.WithStack(err)
		}

		return item.Id, nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// 返回主健
	return &modelx.IDModel{Id: newId.(int64)}, nil
}

// Mod 修改系统用户
//
//	@Summary	修改系统用户
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		model.SysUserModReqModel	true	"修改系统用户参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.RespOptModel}
//	@Router		/system/sysUser/mod [post]
func (h *sysUserHandler) Mod(ctx context.Context, reqModel *model.SysUserModReqModel) (*modelx.RespOptModel, error) {
	// 获取登录用户信息
	uid, ok := contextx.GetTraceUserId(ctx).(int64)
	if !ok {
		return nil, errorx.ErrNonLogin
	}

	// 开启事务
	num, err := zorm.Transaction(ctx, func(ctx context.Context) (any, error) {
		// 数据类型转换
		var item entity.SysUserEntity
		if err := copier.Copy(&item, reqModel); err != nil {
			return nil, errors.WithStack(err)
		}

		// 默认值
		item.UpdatedAt = time.Now()
		item.UpdatedBy = uid

		// 更新数据
		num, err := zorm.UpdateNotZeroValue(ctx, &item)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		return num, nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// 返回影响数据条数
	return &modelx.RespOptModel{Num: num.(int)}, nil
}

// Del 删除系统用户
//
//	@Summary	删除系统用户
//	@Tags		system
//	@Accept		json
//	@Produce	json
//	@Param		body	body		modelx.IDModel	true	"删除系统用户参数"
//	@Success	200		{object}	modelx.RespWrapper{data=modelx.RespOptModel}
//	@Router		/system/sysUser/del [post]
func (h *sysUserHandler) Del(ctx context.Context, reqModel *modelx.IDModel) (*modelx.RespOptModel, error) {
	// 获取登录用户信息
	uid, ok := contextx.GetTraceUserId(ctx).(int64)
	if !ok {
		return nil, errorx.ErrNonLogin
	}

	// 开启事务
	num, err := zorm.Transaction(ctx, func(ctx context.Context) (any, error) {
		// 构造逻辑删除数据
		item := entity.SysUserEntity{
			CommonEntity: dbx.CommonEntity{
				Id:        reqModel.Id,
				DeletedAt: time.Now(),
				DeletedBy: uid,
			},
		}

		// 更新数据
		num, err := zorm.UpdateNotZeroValue(ctx, &item)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		return num, nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// 返回影响数据条数
	return &modelx.RespOptModel{Num: num.(int)}, nil
}
