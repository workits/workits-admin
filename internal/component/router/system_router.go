package router

import (
	"gitee.com/workits/workits-admin/internal/app/system/component/router"

	"github.com/gin-gonic/gin"
)

func (r *Router) BindSystemHandler(group *gin.RouterGroup) {
	systemGroup := group.Group("/system")
	router.R.BindHandler(systemGroup)
}